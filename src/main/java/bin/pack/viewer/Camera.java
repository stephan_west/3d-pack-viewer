package bin.pack.viewer;

import org.joml.Matrix4f;
import org.joml.Quaternionf;
import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;

public class Camera {

	private Vector3f offset;
	private Vector3f position;
	private Quaternionf orientation;
	private Matrix4f viewMatrix;
	private Matrix4f inverseViewMatrix;
	private Vector2i viewSize;
	private Vector2f nearFar;
	private float FOV;
	private Matrix4f projectionMatrix;
	private Matrix4f inverseProjectionMatrix;
	private Boolean invertY;
	private Boolean mouseXRoll;
	
	public Camera(float FOV, Vector2i viewSize, Vector2f nearFar) {
		offset = new Vector3f(0.0f, 0.0f, 0.0f);
		position = new Vector3f(0.0f, 0.0f, 0.0f);
		orientation = new Quaternionf();
		viewMatrix = new Matrix4f(1f, 1f, 1f, 1f,
				1f, 1f, 1f, 1f,
				1f, 1f, 1f, 1f,
				1f, 1f, 1f, 1f);
		inverseViewMatrix = new Matrix4f();
		
		this.FOV = FOV;
		this.viewSize = viewSize;
		this.nearFar = nearFar;
		invertY = false;
		mouseXRoll = false;
	}
	
	public void computeViewMatrix() {
		//convert Quaternion orientation to matrix4f
		Matrix4f rotationMatrix = new Matrix4f().set(orientation.conjugate(new Quaternionf()));
		Matrix4f offsetMatrix = new Matrix4f().translate(offset.negate(new Vector3f()));
		Matrix4f translationMatrix = new Matrix4f().translate(position.negate(new Vector3f()));
		viewMatrix.set(offsetMatrix.mul(rotationMatrix).mul(translationMatrix));  // offsetMatrix * rotationMatrix * translationMatrix;
		viewMatrix.invert(inverseViewMatrix);
	}
	
	public void computeProjectionMatrix() {
		projectionMatrix.setPerspective(FOV, viewSize.x / viewSize.y, nearFar.x, nearFar.y);
		inverseProjectionMatrix.set(projectionMatrix.invert(new Matrix4f()));
	}
	
	public void move(Vector3f movement) {
		position.add(viewMatrix.m00(), viewMatrix.m10(), viewMatrix.m20() * movement.x); // Move X position
		position.add(viewMatrix.m01(), viewMatrix.m11(), viewMatrix.m21() * movement.y); // Move Y position
		position.add(viewMatrix.m02(), viewMatrix.m12(), viewMatrix.m22() * movement.z); // Move Z position
	}
	
	public void rotate(Vector3f angles) {
		orientation.rotateXYZ(angles.x, angles.y, angles.z);
		orientation.normalize();
	}
	
	public void rotateByMouse(Vector2f mouseDeltas) {
		if (!invertY) {
			mouseDeltas.y = -mouseDeltas.x;
		}
		if (mouseXRoll) {
			rotate(new Vector3f(mouseDeltas.y / 1000f, 0f, mouseDeltas.x / 1000f));
		} else {
			rotate(new Vector3f(mouseDeltas.y / 1000f, mouseDeltas.x / 1000f, 0f));
		}
	}
	
	public void update() {
		computeViewMatrix();
	}

	public Vector3f getOffset() {
		return offset;
	}

	public void setOffset(Vector3f offset) {
		this.offset = offset;
	}

	public Quaternionf getOrientation() {
		return orientation;
	}

	public void setOrientation(Quaternionf orientation) {
		this.orientation = orientation;
	}

	public Vector3f getPosition() {
		return position;
	}

	public void setPosition(Vector3f position) {
		this.position = position;
	}

	public Matrix4f getViewMatrix() {
		return viewMatrix;
	}

	public void setViewMatrix(Matrix4f viewMatrix) {
		this.viewMatrix = viewMatrix;
	}

	public Matrix4f getInverseViewMatrix() {
		return inverseViewMatrix;
	}

	public void setInverseViewMatrix(Matrix4f inverseViewMatrix) {
		this.inverseViewMatrix = inverseViewMatrix;
	}

	public Vector2i getViewSize() {
		return viewSize;
	}

	public void setViewSize(Vector2i viewSize) {
		this.viewSize = viewSize;
	}

	public Vector2f getNearFar() {
		return nearFar;
	}

	public void setNearFar(Vector2f nearFar) {
		this.nearFar = nearFar;
	}

	public float getFOV() {
		return FOV;
	}

	public void setFOV(float fOV) {
		FOV = fOV;
	}

	public Matrix4f getProjectionMatrix() {
		return projectionMatrix;
	}

	public void setProjectionMatrix(Matrix4f projectionMatrix) {
		this.projectionMatrix = projectionMatrix;
	}

	public Matrix4f getInverseProjectionMatrix() {
		return inverseProjectionMatrix;
	}

	public void setInverseProjectionMatrix(Matrix4f inverseProjectionMatrix) {
		this.inverseProjectionMatrix = inverseProjectionMatrix;
	}

	public Boolean getInvertY() {
		return invertY;
	}

	public void setInvertY(Boolean invertY) {
		this.invertY = invertY;
	}

	public Boolean getMouseXRoll() {
		return mouseXRoll;
	}

	public void setMouseXRoll(Boolean mouseXRoll) {
		this.mouseXRoll = mouseXRoll;
	}
}
