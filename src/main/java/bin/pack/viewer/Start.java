package bin.pack.viewer;

import org.joml.Vector2f;
import org.joml.Vector3f;
import static org.lwjgl.glfw.GLFW.*;

import java.util.ArrayList;

public class Start implements ILogic {
	
	private static final float MOUSE_SENSITIVITY = 0.2f;

    private int displxInc = 0;

    private int displyInc = 0;

    private int displzInc = 0;

    private int scaleInc = 0;

    private int direction = 0;

    private float color = 0.0f;

    private final Renderer renderer;
    
    private ArrayList<Package> myPackages;
    
    private Camera camera;
    
    private Vector3f cameraInc;
    
    public Start() {
        renderer = new Renderer();
        myPackages = new ArrayList<Package>();
        cameraInc = new Vector3f(0.0f, 0.0f, 0.0f);
    }
    
    @Override
    public void init(Window window) throws Exception {
        renderer.init(window);
        camera = new Camera(window.FOV, window.getWindowSize(), window.getNearFar());
        Package pack = new Package("Eggs", 0, 0, 0, 50, 50, 50);
        pack.setPosition(0, 0, -5);
        myPackages.add(pack);
    }

    @Override
    public void input(Window window, MouseInput mouseInput) {
    	// Get camera and keyboard input and transform the camera
    	cameraInc.set(0.0f, 0.0f, 0.0f);
        displyInc = 0;
        displxInc = 0;
        displzInc = 0;
        scaleInc = 0;
        if (window.isKeyPressed(GLFW_KEY_UP)) {
            cameraInc.z = -1;
        } else if (window.isKeyPressed(GLFW_KEY_DOWN)) {
            cameraInc.z = 1;
        } else if (window.isKeyPressed(GLFW_KEY_LEFT)) {
            cameraInc.x = -1;
        } else if (window.isKeyPressed(GLFW_KEY_RIGHT)) {
            cameraInc.x = 1;
        }
    }


    @Override
    public void update(float interval, MouseInput mouseInput, Window window) {
    	Vector2f rotVec = new Vector2f(mouseInput.getDisplVec().x * MOUSE_SENSITIVITY, 
    			mouseInput.getDisplVec().y * MOUSE_SENSITIVITY);
    	camera.rotateByMouse(rotVec);
    	camera.move(cameraInc);
    	camera.update();
    }

    @Override
    public void render(Window window) {
        renderer.render(window, camera, myPackages);
    }

    @Override
    public void cleanup() {
        renderer.cleanup();
        for (Package pack: myPackages) {
        	pack.cleanup();
        }
    }

}