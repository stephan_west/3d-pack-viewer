package bin.pack.viewer;

// See samples from: https://github.com/lwjglgamedev/lwjglbook/blob/master/chapter06/c06-p2

public class Main {

	public static void main(String[] args) {
	       try {
	            boolean vSync = true;
	            ILogic gameLogic = new Start();
	            Engine gameEng = new Engine("GAME", 600, 480, vSync, gameLogic);
	            gameEng.run();
	        } catch (Exception excp) {
	            excp.printStackTrace();
	            System.exit(-1);
	        }
	}

}
