package bin.pack.viewer;

import org.joml.Quaternionf;
import org.joml.Vector3f;

public class Package {

	private Mesh mesh;
	private String packageName;
	private final Vector3f position;
    
    private float scale;

    private final Quaternionf rotation;
	
	public Package (String name, int x, int y, int z, int width, int height, int length) {
		packageName = name;
		position = new Vector3f(0, 0, 0);
		rotation = new Quaternionf();
		scale = 1;
		float[] positions = new float[]{	
		                     0, 0, 0,
		                     0, height, 0,
		                     0, height, width,
		                     0, 0, width,
		                     length, 0, width,
		                     length, 0, 0,
		                     length, height, 0,
		                     length, height, width
		};
		float[] colors = new float[]{
                1, 0, 0,
                1, 0, 0,
                1, 0, 0,
                1, 0, 0,
                1, 0, 0,
                1, 0, 0,
                1, 0, 0,
                1, 0, 0
		};
		int[] indices = new int[]{
			0, 1, 2, 
			2, 3, 0,
			0, 5, 6,
			6, 1, 0,
			0, 5, 4,
			4, 3, 0,
			1, 6, 7,
			7, 2, 1,
			2, 7, 4,
			4, 3, 2,
			2, 7, 6,
			6, 1, 2,
			5, 4, 7,
			7, 6, 5
		};
		mesh = new Mesh(positions, colors, indices);
	}
	
	public Mesh getMesh() {
		return mesh;
	}
	
	public  void cleanup() {
		mesh.cleanUp();
	}
	
    public Vector3f getPosition() {
        return position;
    }

    public void setPosition(float x, float y, float z) {
        this.position.x = x;
        this.position.y = y;
        this.position.z = z;
    }
    
    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }

    public Quaternionf getRotation() {
        return rotation;
    }

    public void setRotation(Quaternionf q) {
    	this.rotation.set(q);
    }
}
