package bin.pack.viewer;

import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;

import junit.framework.TestCase;

public class CameraTest extends TestCase {

	public static void testCameracomputeViewMatrix() {
		Camera camera = new Camera(60, new Vector2i(800, 600), new Vector2f(0.001f, 1000.0f));
		camera.move(new Vector3f(20f, 0f, 0f));
		camera.computeViewMatrix();
		Matrix4f viewMatrix = new Matrix4f();
		Matrix4f inverseViewMatrix = new Matrix4f();
		assertEquals(camera.getViewMatrix(), viewMatrix);
		assertEquals(camera.getInverseViewMatrix(), inverseViewMatrix);
		assertEquals(true, false);
	}
	public static void testCameramove() {
		Camera camera = new Camera(60, new Vector2i(800, 600), new Vector2f(0.001f, 1000.0f));
		camera.setPosition(new Vector3f(0f, 0f, 0f));
		camera.move(new Vector3f(20f, 0f, 0f));
		Vector3f position = new Vector3f(20f, 0f, 0f);
		assertEquals(position, camera.getPosition());
	}
}
